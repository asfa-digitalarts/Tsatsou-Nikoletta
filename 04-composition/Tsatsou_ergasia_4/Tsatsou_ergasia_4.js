let img;
let img2;
let img3;
let img4;
let img5;
let img6;
let img7;
let song;
let song2;
let song3;
let song4;
let song5;
let song6;
let song7;

function preload() {
  img = loadImage('data/background.jpg');
  img2 = loadImage('data/1.jpg');
  img3 = loadImage('data/2.jpg');
  img4 = loadImage('data/3.jpg');
  img5 = loadImage('data/4.jpg');
  img6 = loadImage('data/5.jpg');
  img7 = loadImage('data/6.jpg');
  song = loadSound('data/all4.wav');
  song2 = loadSound('data/1e.wav');
  song3 = loadSound('data/2b.wav');
  song4 = loadSound('data/3g.wav');
  song5 = loadSound('data/4d.wav');
  song6 = loadSound('data/5a.wav');
  song7 = loadSound('data/6e.wav');
  
}

function setup() {
  createCanvas(1400, 1000);
  image(img, width/3, 0);

}


function draw() {

}

function keyTyped() {
  if (key === 'a') {
    song2.stop();
    song3.stop();
    song4.stop();
    song5.stop();
    song6.stop();
    song7.stop();
    song.play();
  } else if (key === '1') {
    song2.play();
    image(img2, 233, 0);
  } else if (key === '2') {
    song3.play();
    image(img3, 233, 166); 
  } else if (key === '3') {
    song4.play();
    image(img4, 233, 332);
  } else if (key === '4') {
    song5.play();
    image(img5, 1165, 0);
  } else if (key === '5') {
    song6.play();
    image(img6, 1165, 166);
  } else if (key === '6') {
    song7.play();
    image(img7, 1165, 332);
  }
}

function draw() {
  song.rate(map(mouseX, 0, width, 0,2, 50));

}
