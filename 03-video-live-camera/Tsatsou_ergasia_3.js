let amelie;
let mew;
let ninikos;
let playing1 = false;
let playing2 = false;
let playing3 = false;
let button;
let button2;
let button3;

//function preload() {
  //amelie = createVideo(['data/amelie.mp4', 'assets/amelie.ogv', 'assets/amelie.webm']);
  //mew = createVideo(['data/mew.mp4', 'assets/mew.ogv', 'assets/mew.webm']);
  //ninikos = createVideo(['data/ninikos.mp4', 'assets/ninnikos.ogv', 'assets/ninikos.webm']);
//}

function setup() {
  amelie = createVideo(['data/amelie.mp4', 'assets/amelie.ogv', 'assets/amelie.webm']);
  mew = createVideo(['data/mew.mp4', 'assets/mew.ogv', 'assets/mew.webm']);
  ninikos = createVideo(['data/ninikos.mp4', 'assets/ninnikos.ogv', 'assets/ninikos.webm']);
  amelie.volume(0);
  mew.volume(0);
  ninikos.volume(0);
  button = createButton('Ninikos');
  button.mousePressed(toggleVid1);
  button2 = createButton('Amelie');
  button2.mousePressed(toggleVid2);
  button2.position(100,650);
  button3 = createButton('Mew');
  button3.mousePressed(toggleVid3);
  button3.position(850,10);

}


function draw() {

}

function toggleVid1() {
  if (playing1) {
    ninikos.pause();
    button.html('Ninikos');
    } else {
    ninikos.loop();
    button.html('pause');
  }
  playing1 = !playing1;
}

function toggleVid2() {
  if (playing2) {
    amelie.pause();
    button2.html('Amelie');
    } else {
    amelie.loop();
    button2.html('pause');
  }
  playing2 = !playing2;
    
}

function toggleVid3() {
  if (playing3) {
    mew.pause();
    button3.html('Mew');
    } else {
    mew.loop();
    button3.html('pause');
  }
  playing3 = !playing3;
    
}
